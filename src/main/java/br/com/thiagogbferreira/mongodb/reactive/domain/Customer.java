package br.com.thiagogbferreira.mongodb.reactive.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Document
@AllArgsConstructor
@ToString
@EqualsAndHashCode(of = "document")
@Data
public class Customer {

  @Id
  private String document;
  private String name;


}
