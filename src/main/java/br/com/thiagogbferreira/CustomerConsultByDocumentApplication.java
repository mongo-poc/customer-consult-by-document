package br.com.thiagogbferreira;

import br.com.thiagogbferreira.mongodb.reactive.domain.Customer;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

@SpringBootApplication
@EnableMongoAuditing
@Slf4j
public class CustomerConsultByDocumentApplication {

  @Autowired
  private CustomerRepository customerRepository;

  public static void main(String[] args) {
    SpringApplication.run(CustomerConsultByDocumentApplication.class, args);
  }

  @Bean
  RouterFunction<?> routerFunction(CustomerConsultByDocumentApplication routerHandlers) {
    return RouterFunctions
        .route(
            RequestPredicates.GET("/customers/{document}"),
            routerHandlers::getCustomerByDocument
        );
  }

  public Mono<ServerResponse> getCustomerByDocument(ServerRequest serverRequest) {
    String document = serverRequest.pathVariable("document");
    MDC.put("key", document);
    log.info("find customer by {}", document);
    return ServerResponse.ok().body(customerRepository.findById(document),Customer.class);
  }
}

interface CustomerRepository extends ReactiveMongoRepository<Customer, String> {
}
